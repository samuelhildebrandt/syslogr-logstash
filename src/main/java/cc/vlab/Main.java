package cc.vlab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static cc.vlab.Foo.foo;
import static net.logstash.logback.argument.StructuredArguments.array;
import static net.logstash.logback.argument.StructuredArguments.entries;
import static net.logstash.logback.argument.StructuredArguments.fields;
import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static net.logstash.logback.argument.StructuredArguments.value;

public class Main {

    private static Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        // Standard logging
        log.trace("trace msg");
        log.debug("debug msg");
        log.info("info msg");
        log.warn("warn msg");
        log.error("error msg");

        // Log exception
        try {
            throw new Exception("first exception");
        } catch (Exception e) {
            log.warn("Failed", new Exception("second exception", e));
        }


        // https://github.com/logstash/logstash-logback-encoder/blob/master/README.md

        /*
         * Add "name":"value" to the JSON output,
         * but only add the value to the formatted message.
         *
         * The formatted message will be `log message value`
         */
        log.info("log message {}", value("name", "value"));

        /*
         * Add "name":"value" to the JSON output,
         * and add name=value to the formatted message.
         *
         * The formatted message will be `log message name=value`
         */
        log.info("log message {}", keyValue("name", "value"));

        /*
         * Add "name":"value" ONLY to the JSON output.
         *
         * Since there is no parameter for the argument,
         * the formatted message will NOT contain the key/value.
         *
         * If this looks funny to you or to static analyzers,
         * consider using Markers instead.
         */
        log.info("log message", keyValue("name", "value"));

        /*
         * Add multiple key value pairs to both JSON and formatted message
         */
        log.info("log message {} {}", keyValue("name1", "value1"), keyValue("name2", "value2"));

        /*
         * Add "name":"value" to the JSON output and
         * add name=[value] to the formatted message using a custom format.
         */
        log.info("log message {}", keyValue("name", "value", "{0}=[{1}]"));

        /*
         * In the JSON output, values will be serialized by Jackson's ObjectMapper.
         * In the formatted message, values will follow the same behavior as logback
         * (formatting of an array or if not an array `toString()` is called).
         *
         * Add "foo":{...} to the JSON output and add `foo.toString()` to the formatted message:
         *
         * The formatted message will be `log message <result of foo.toString()>`
         */
        Foo foo = new Foo("name of foo", Arrays.asList("bar1", "bar2"));
        log.info("log message {}", value("foo", foo));

        /*
         * Add "name1":"value1","name2":"value2" to the JSON output by using a Map,
         * and add `myMap.toString()` to the formatted message.
         *
         * Note the values can be any object that can be serialized by Jackson's ObjectMapper
         * (e.g. other Maps, JsonNodes, numbers, arrays, etc)
         */
        Map<String, String> myMap = new HashMap<>();
        myMap.put("name1", "value1");
        myMap.put("name2", "value2");
        log.info("log message {}", entries(myMap));

        /*
         * Add "array":[1,2,3] to the JSON output,
         * and array=[1,2,3] to the formatted message.
         */
        log.info("log message {}", array("array", 1, 2, 3));

        /*
         * Add fields of any object that can be unwrapped by Jackson's UnwrappableBeanSerializer to the JSON output.
         * i.e. The fields of an object can be written directly into the JSON output.
         * This is similar to the @JsonUnwrapped annotation.
         *
         * The formatted message will contain `myobject.toString()`
         */
        log.info("log message {}", fields(foo));

        /*
         * In order to normalize a field object name, static helper methods can be created.
         * For example, `foo(Foo)` calls `value("foo" , foo)`
         */
        log.info("log message {}", foo(foo));
    }
}
