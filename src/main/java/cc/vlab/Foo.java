package cc.vlab;

import net.logstash.logback.argument.StructuredArgument;

import java.util.List;

import static net.logstash.logback.argument.StructuredArguments.*;

public class Foo {

    private String name;
    private List<String> bars;

    public Foo(String name, List<String> bars) {
        this.name = name;
        this.bars = bars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getBars() {
        return bars;
    }

    public void setBars(List<String> bars) {
        this.bars = bars;
    }

    public static StructuredArgument foo(Foo foo) {
        return keyValue("foo", foo, "{1}");
    }
}
